package test1;

public class Result {
	
	public void print()
	{
		for(ResultSymbol r:_result)
		{
			if(r != null)
			{
				r.print();
			}
		}
	}
	
	public void checkSymbol(int idx, char s, int i, int j)
	{
		if(idx >= 0 && idx < _result.length)
		{
			if(_result[idx] == null)
			{
				_result[idx] = new ResultSymbol(s, i, j);
				numSymbols++;
			}
		}
	}
	
	public boolean checkFilled()
	{
		return numSymbols == 10;
	}
	
	private ResultSymbol[] _result = new ResultSymbol[10];//OneTwoTrip count	
	int numSymbols = 0;
}
