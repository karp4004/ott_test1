package test1;

import java.util.ArrayList;
import java.util.Arrays;

public class SymbolTable {
	public SymbolTable()
	{
		keyWord = new SymbolPosition[256];//ASCII table, OneTwoTrip - ASCII symbols
		keyWord[0x6f] = new SymbolPosition(new int[]{0,5}); 
		keyWord[0x6e] = new SymbolPosition(new int[]{1}); 
		keyWord[0x65] = new SymbolPosition(new int[]{2}); 
		keyWord[0x74] = new SymbolPosition(new int[]{3,6}); 
		keyWord[0x77] = new SymbolPosition(new int[]{4});
		keyWord[0x72] = new SymbolPosition(new int[]{7});
		keyWord[0x69] = new SymbolPosition(new int[]{8});
		keyWord[0x70] = new SymbolPosition(new int[]{9});
	}
	
	public void checkSymbol(char idx, int i, int j, ArrayList<Result> _result)
	{
		if(idx > 0 && idx < keyWord.length)
		{
			SymbolPosition data = keyWord[Character.toLowerCase(idx)];
			if(data != null)
			{
				data.checkSymbol(idx, i, j, _result);
			}
		}
	}
	
	private SymbolPosition[] keyWord;
}
