package test1;

import java.util.ArrayList;

public class SymbolPosition {
	public SymbolPosition(int[] arr) {
		_positions = arr;
	}

	public void checkSymbol(char s, int i, int j, ArrayList<Result> _result) {
		if (_pisitionIndex < _positions.length) {
			if (_resultIndex >= _result.size()) {
				_result.add(new Result());
			}

			Result r = _result.get(_resultIndex);
			if (r != null) {
				r.checkSymbol(_positions[_pisitionIndex], s, i, j);
			}
		}

		_pisitionIndex++;

		if (_pisitionIndex >= _positions.length) {
			_pisitionIndex = 0;
			_resultIndex++;
		}
	}

	private int[] _positions;
	private int _pisitionIndex = 0;
	private int _resultIndex = 0;
}
