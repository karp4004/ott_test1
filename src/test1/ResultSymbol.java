package test1;

public class ResultSymbol {
	public ResultSymbol(char sym, int i, int j)
	{
		_symbol = sym;
		_c = i;
		_r = j;
	}
	
	public void print()
	{
		System.out.println(String.format("%c - (%d,%d);", _symbol, _c, _r));
	}
	
	private char _symbol;
	private int _c;
	private int _r;
}
