package test1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class test1 {
	
	public static void readSymbols(BufferedReader file, SymbolTable _symbolTable, ArrayList<Result> _result) throws IOException
	{	
		char[] block = new char[256];
		Arrays.fill(block, '\0');
		
		int c=0;
		int r=0;
		while(true)
		{	
			int ret = file.read(block);			
			if(ret != -1)
			{
				for(int i=0;i<block.length;i++)
				{
					if(block[i] == '\n')
					{
						r++;
						c = 0;
					}
					else
					{
						_symbolTable.checkSymbol(block[i], r, c, _result);								
						c++;
					}
				}				
			}
			else
			{					
				int cnt = 0;
				for(Result res: _result)
				{
					if(res.checkFilled())
					{
						cnt++;
						
						System.out.println(String.format("word:%d", cnt));
						res.print();
						System.out.println("\n");						
					}
				}				
				
				if(cnt < 1)
				{
					System.out.println(String.format("Impossible"));
				}
				
				return;
			}		
		}	
	}
	
	public static void main(String[] args) {		
		BufferedReader file = null;		
		try {	
			String curDir = System.getProperty("user.dir") + "\\input.txt";
			file = new BufferedReader(new FileReader(curDir));
			
			String line =  file.readLine();					
			if(line != null)
			{						
				readSymbols(file, new SymbolTable(), new ArrayList<Result>());
			}			
		} catch (FileNotFoundException e) {
			System.out.println("" + e);
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("" + e);
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("" + e);
			e.printStackTrace();
		} finally {
			try {
				if (file != null) {
					file.close();
				}				
			} catch (IOException e) {
				System.out.println("" + e);
				e.printStackTrace();
			}
		}
	}
}
